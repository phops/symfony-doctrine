
Phops Symfony Doctrine
======================


Basic usage
-----------

```bash
composer require phops/symfony-doctrine
```


License
-------

Phops Symfony Doctrine is licensed under the [MIT license](/license.txt).
