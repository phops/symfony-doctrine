<?php

namespace Phops\SymfonyDoctrine;

use \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use \Symfony\Component\DependencyInjection\Compiler\PassConfig;
use \Symfony\Component\DependencyInjection\ContainerBuilder;
use \Symfony\Component\DependencyInjection\Definition;
use \Symfony\Component\DependencyInjection\Reference;

class DBALConnection extends \Doctrine\DBAL\Connection {

  static function registerBundles () {
    return [new \Phops\SymfonyDoctrine\DoctrineMigrationsBundle([
      'connection_class' => static::class,
      'migrations_directories' => [
        'App\Migrations' => 'src/Migrations',
      ],
    ])];
  }

  static function buildContainer (ContainerBuilder $builder) {

    $builder->addCompilerPass(new class (static::class) implements CompilerPassInterface {
      function __construct ($class) {
        $this->class = $class;
      }
      function process (ContainerBuilder $builder): void {
        $builder->getDefinition($this->class)->setFactory([$this->class, 'createConnection']);
      }
    }, PassConfig::TYPE_BEFORE_OPTIMIZATION, -10000);

    $definition = new Definition(\Phops\SymfonyDoctrine\DBALConnectionCheck::class);
    $definition->setArguments([new Reference(static::class)]);
    $definition->setAutowired(true);
    $definition->setAutoconfigured(true);
    $builder->setDefinition(\Phops\SymfonyDoctrine\DBALConnectionCheck::class, $definition);

  }


  static function createConnection () {
    $url = static::getURL();
    return new static(
      [
        'url' => $url,
        'driver' => rawurldecode(parse_url($url, PHP_URL_SCHEME)),
        'host' => rawurldecode(parse_url($url, PHP_URL_HOST)),
        'port' => parse_url($url, PHP_URL_PORT) === null ? 5432 : rawurldecode(parse_url($url, PHP_URL_PORT)),
        'user' => parse_url($url, PHP_URL_USER) === null ? null : rawurldecode(parse_url($url, PHP_URL_USER)),
        'password' => parse_url($url, PHP_URL_PASS) === null ? null : rawurldecode(parse_url($url, PHP_URL_PASS)),
        'dbname' => substr(rawurldecode(parse_url($url, PHP_URL_PATH)), 1),
      ],
      new \Doctrine\DBAL\Driver\PDOPgSql\Driver(),
      new \Doctrine\DBAL\Configuration(),
      new \Doctrine\Common\EventManager(),
    );
  }

  static function getURL () {
    return '';
  }

}
