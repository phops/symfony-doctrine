<?php

namespace Phops\SymfonyDoctrine;

use \Doctrine\DBAL\Connection;
use \Exception;
use \Symfony\Component\Console\Formatter\OutputFormatter;
use \Symfony\Component\Console\Output\OutputInterface;

class DBALConnectionCheck {

  static $serviceTags = ['kernel.env_check'];

  /** @var OutputInterface */
  protected $output;

  function setOutput ($output) {
    $this->output = $output;
  }

  /** @var Connection */
  protected $db;

  function __construct (Connection $db) {
    $this->db = $db;
  }

  function run () {
    $this->output->write(
      'Checking database connection (<fg=cyan>'
      . OutputFormatter::escape(
        urlencode(parse_url($this->db->getParams()['url'], PHP_URL_SCHEME))
        . '://' . urlencode($this->db->getUsername())
        . '@' . urlencode($this->db->getHost())
        . ':' . urlencode($this->db->getPort())
        . '/' . urlencode($this->db->getDatabase())
        . (parse_url($this->db->getParams()['url'], PHP_URL_QUERY)
          ? '?' . parse_url($this->db->getParams()['url'], PHP_URL_QUERY) : '')
      )
      . '</>) ...'
    );

    $timeout = microtime(true) + 15;

    while (true) {
      try {
        $dbinfo = $this->db->executeQuery('
          select encoding, datcollate, datctype from pg_database where datname = :database;
        ', [
          'database' => $this->db->getDatabase(),
        ])->fetch();
        break;
      } catch (Exception $exception) {
        if (microtime(true) > $timeout) {
          throw $exception;
        }
        usleep(0.5 * 1000 * 1000);
        $this->output->write('.');
      }
    }

    if (!$dbinfo) {
      throw new Exception('Database not found.');
    }
    if ($dbinfo['encoding'] != 6)
      throw new Exception('Unexpected database encoding.');

    if (strtolower($dbinfo['datcollate']) != strtolower('en_US.UTF8'))
      throw new Exception('Unexpected database collate.');

    if (strtolower($dbinfo['datctype']) != strtolower('en_US.UTF8'))
      throw new Exception('Unexpected database ctype.');

    $this->output->writeln(' <fg=green>ok</>.');
  }

}
