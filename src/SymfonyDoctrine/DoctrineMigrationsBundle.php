<?php

namespace Phops\SymfonyDoctrine;

use \Doctrine\Migrations\Configuration\Configuration as MigrationsConfiguration;
use \Doctrine\Migrations\Configuration\Connection\ExistingConnection;
use \Doctrine\Migrations\Configuration\Migration\ExistingConfiguration;
use \Doctrine\Migrations\DependencyFactory;
use \Doctrine\Migrations\Metadata\Storage\TableMetadataStorageConfiguration;
use \Phops\URL;
use \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use \Symfony\Component\DependencyInjection\ContainerBuilder;
use \Symfony\Component\DependencyInjection\Definition;
use \Symfony\Component\DependencyInjection\Reference;

class DoctrineMigrationsBundle extends \Symfony\Component\HttpKernel\Bundle\Bundle implements CompilerPassInterface {

  protected $parameters = [];

  function __construct ($parameters = []) {
    $this->parameters = $parameters;
  }

  function build (ContainerBuilder $builder) {
    $builder->addCompilerPass($this);
  }

  function process (ContainerBuilder $builder): void {

    $definition = $builder->getDefinition($this->parameters['connection_class']);
    $definition->setPublic(true);
    $builder->setDefinition($this->parameters['connection_class'], $definition);

    $definition = new Definition(MigrationsConfiguration::class);
    $definition->setFactory([__CLASS__, 'createMigrationsConfiguration']);
    $definition->setArguments([new Reference('kernel'), $this->parameters['migrations_directories']]);
    $definition->setAutowired(true);
    $definition->setAutoconfigured(true);
    $builder->setDefinition(MigrationsConfiguration::class, $definition);

    $definition = new Definition(DependencyFactory::class);
    $definition->setFactory([__CLASS__, 'createDependencyFactory']);
    $definition->setArguments([
      new Reference('kernel'),
      new Reference(MigrationsConfiguration::class),
      $this->parameters['connection_class'],
    ]);
    $definition->setAutowired(true);
    $definition->setAutoconfigured(true);
    $builder->setDefinition('doctrine.migrations.dependency_factory', $definition);

    foreach ([
      \Doctrine\Migrations\Tools\Console\Command\DumpSchemaCommand::class,
      \Doctrine\Migrations\Tools\Console\Command\ExecuteCommand::class,
      \Doctrine\Migrations\Tools\Console\Command\GenerateCommand::class,
      \Doctrine\Migrations\Tools\Console\Command\LatestCommand::class,
      \Doctrine\Migrations\Tools\Console\Command\ListCommand::class,
      \Doctrine\Migrations\Tools\Console\Command\MigrateCommand::class,
      \Doctrine\Migrations\Tools\Console\Command\RollupCommand::class,
      \Doctrine\Migrations\Tools\Console\Command\StatusCommand::class,
      \Doctrine\Migrations\Tools\Console\Command\SyncMetadataCommand::class,
      \Doctrine\Migrations\Tools\Console\Command\VersionCommand::class,
    ] as $class) {
      $definition = new Definition($class);
      $definition->setArguments([new Reference('doctrine.migrations.dependency_factory')]);
      $definition->setAutowired(true);
      $definition->setAutoconfigured(true);
      $definition->addTag('console.command');
      $builder->setDefinition($class, $definition);
    }

  }

  static function createMigrationsConfiguration ($kernel, $migrationsDirectories) {
    $configuration = new MigrationsConfiguration();
    foreach ($migrationsDirectories as $namespace => $directory)
      $configuration->addMigrationsDirectory($namespace, URL::follow($kernel->getProjectDir(), $directory));
    $storageConfiguration = new TableMetadataStorageConfiguration();
    $storageConfiguration->setTableName('migration');
    $configuration->setMetadataStorageConfiguration($storageConfiguration);
    return $configuration;
  }

  static function createDependencyFactory ($kernel, MigrationsConfiguration $configuration, $connectionClass) {
    return DependencyFactory::fromConnection(
      new ExistingConfiguration($configuration),
      new ExistingConnection($kernel->getContainer()->get($connectionClass)),
    );
  }

}
